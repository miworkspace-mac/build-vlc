#!/bin/bash -ex

# CONFIG
prefix="VLC"
suffix=""
munki_package_name="VLC"
display_name="VLC Media Player"
icon_name=""
category="Video"
description="VLC Media Player is a highly portable multimedia player for various audio and video formats (MPEG-1, MPEG-2, MPEG-4, DivX, MP3, OGG, ...) as well as DVDs, VCDs, and various streaming protocols."
url=`./vlcfinder.sh`

# download it
curl -L -o app.dmg $url

#mount downloaded DMG
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

#extract .app and move to build-root
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" $app_in_dmg/Contents/Info.plist`

/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist

plutil -replace BundleIsRelocatable -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

rm -rf Component-${munki_package_name}.plist

# Build pkginfo
/usr/local/munki/makepkginfo app.pkg -f build-root/Applications/*.app > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" unattended_install -bool TRUE

# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"



# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

