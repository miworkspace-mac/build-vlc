#!/bin/bash

NEWLOC=`curl -L http://www.videolan.org/vlc/download-macosx.html  2>/dev/null | /usr/local/bin/htmlq -a href a | grep universal.dmg | head -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo "http:"${NEWLOC}
fi